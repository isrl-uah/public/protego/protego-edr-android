# ProTego CA Android APK


## Merge

1. Include the application as a library (or copy `src/main/**`, except `AndroidManifest.xml`)

2. Include gradle dependencies (`build.gradle`)

3. Add the code contained by `<application>` label (including its attributes) to your manifest:

```xml
<application
    android:allowBackup="true"
    android:icon="@drawable/protego_logo_color"
    ...
    android:networkSecurityConfig="@xml/network_security_config"
    android:supportsRtl="true">

    ...

</application>
```

4. Mix these files:

- res/xml/network-security.xml

- res/xml/policies.xml

- res/values/strings.xml

5. Mix /gradle.properties and add properties to app/build.gradle (`buildConfigField("String", "PROTEGO_EDR_API", PROTEGO_EDR_API)`)

6. Add somewhere a call to the service: `ProTegoCoreServiceReceiver.call(getApplicationContext());`

## Acknowledgements

This project has received funding from the European Union’s Horizon 2020 Research and innovation programme under grant agreement No. 826284.

![doc/img/protego_logo.png]

# References 

### Worker

- https://developer.android.com/topic/libraries/architecture/workmanager/how-to/define-work

- https://developer.android.com/training/multiple-threads/create-threadpool

- https://developer.android.com/training/multiple-threads/define-runnable

### Sensors

- https://github.com/SensingKit/SensingKit-Android

- https://github.com/itzmeanjan/sensorz

- https://developer.android.com/studio/profile/monitor

- https://developer.android.com/guide/topics/ui/accessibility/service

- https://developer.android.com/guide/topics/sensors

### Other

- https://github.com/google/gson/blob/master/UserGuide.md#using-gson

- https://guides.codepath.com/android/sending-and-managing-network-requests#permissions

- https://developer.android.com/training/volley/index.html

- https://developer.android.com/guide/components/services

- https://developer.android.com/guide/background

- https://stackoverflow.com/questions/16950150/best-way-to-send-http-get-requests-ansynchronously-in-android

- https://stackoverflow.com/questions/12195015/java-annotation-to-execute-some-code-before-and-after-method

- [Hooking Java methods and native functionsto enhance Android applications security](https://amslaurea.unibo.it/12257/1/Brandolini_HookingJavaMethodsAndNativeFunctions.pdf)

### Auth

- https://code.tutsplus.com/tutorials/keys-credentials-and-storage-on-android--cms-30827

- https://github.com/segunfamisa/android-jwt-authentication/blob/master/app/src/main/java/com/segunfamisa/sample/jwt/AuthHelper.java

- https://tech.just-eat.com/2019/12/04/lessons-learned-from-handling-jwt-on-mobile/

- https://stormpath.com/blog/the-ultimate-guide-to-mobile-api-security

- https://auth0.com/docs/architecture-scenarios/mobile-api/mobile-implementation-android

- https://developer.okta.com/books/api-security/

- https://gist.github.com/dmytrodanylyk/e41a4940f1c602868401

### Network security

- https://developer.android.com/training/articles/security-config#CleartextTrafficPermitted

### Device admin

- https://medium.com/@ssaurel/creating-a-lock-screen-device-app-for-android-4ec6576b92e0

- https://developer.android.com/guide/topics/admin/device-admin

# Acknowledgements

This project has received funding from the European Union’s Horizon 2020 Research and innovation programme under grant agreement No. 826284.

![doc/img/protego_logo.png](doc/img/protego_logo.png)

