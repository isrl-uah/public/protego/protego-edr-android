-[ ] Refactor and join android-api common resources (join with a gradle common project?)
-[ ] CHANGE AUTH TO OAUTH: https://tech.just-eat.com/2019/12/04/lessons-learned-from-handling-jwt-on-mobile/
-[ ] Manifest
-[ ] Device id
-[ ] Check component statuses
-[ ] Just notify when not logged, not allways open the Login activity (or limit the times it gets opened)

-- NEW

1. Receive events and send to the server
2. Every 5 minutes, update Trust (TrustAlarmReceiver)
