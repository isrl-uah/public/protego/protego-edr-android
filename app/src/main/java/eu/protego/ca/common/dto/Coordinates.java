package eu.protego.ca.common.dto;

import org.json.JSONException;
import org.json.JSONObject;

import lombok.Data;

@Data
public class Coordinates implements ProTegoDto<Coordinates> {

    double x, y;
    private double timestamp;

    @Override
    public JSONObject toJson() throws JSONException {

        JSONObject res = new JSONObject();

        res.put("x", this.x);
        res.put("y", this.y);
        res.put("timestamp", this.timestamp);

        return res;

    }

    @Override
    public Coordinates fromJson(JSONObject json) throws JSONException, DtoBuilder.DtoBuildException {

        this.x = json.getDouble("x");
        this.y = json.getDouble("y");
        this.timestamp = json.getDouble("timestamp");

        return this;

    }

}
