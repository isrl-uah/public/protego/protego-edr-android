package eu.protego.ca.common.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;

public class DtoBuilder<T> {

    ProTegoDto<T> t;

    private DtoBuilder(Class<T> cls) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        T t = cls.getConstructor().newInstance();
        this.t = (ProTegoDto<T>) t;
    }

    public static <K> DtoBuilder<K> builder(Class<K> k) throws DtoBuildException {
        try {
            return new DtoBuilder<K>(k);
        } catch (Exception e) {
            throw new DtoBuildException();
        }
    }

    public T fromJson(JSONObject json) throws DtoBuildException, JSONException {
        t.fromJson(json);
        return (T) t;
    }

    public static class DtoBuildException extends Exception {}
}
