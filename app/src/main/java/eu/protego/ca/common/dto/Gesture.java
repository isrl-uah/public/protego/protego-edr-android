package eu.protego.ca.common.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import lombok.Data;

@Data
public class Gesture implements ProTegoDto<Gesture>, Serializable {

    private int gestureCode;
    private double timestamp;

    @Override
    public JSONObject toJson() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("gestureCode", gestureCode);
        jsonObject.put("timestamp", timestamp);

        return jsonObject;
    }

    @Override
    public Gesture fromJson(JSONObject json) throws JSONException {
        this.gestureCode = json.getInt("gestureCode");
        this.timestamp = json.getDouble("timestamp");
        return this;
    }

}
