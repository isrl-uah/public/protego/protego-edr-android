package eu.protego.ca.common.dto;

import lombok.Data;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

@Data
public class KeyRelease implements ProTegoDto<KeyRelease>, Serializable {

    private int keyCode;
    private double timestamp;

    @Override
    public JSONObject toJson() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("keyCode", keyCode);
        jsonObject.put("timestamp", timestamp);

        return jsonObject;
    }

    @Override
    public KeyRelease fromJson(JSONObject json) throws JSONException {
        this.keyCode = json.getInt("keyCode");
        this.timestamp = json.getDouble("timestamp");
        return this;
    }
}
