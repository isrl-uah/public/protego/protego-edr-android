package eu.protego.ca.common.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public interface ProTegoDto<T> extends Serializable {

    JSONObject toJson() throws JSONException;

    T fromJson(JSONObject json) throws JSONException, DtoBuilder.DtoBuildException;

}
