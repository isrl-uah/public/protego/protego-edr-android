package eu.protego.ca.common.dto;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import lombok.Data;

@Data
public class ProTegoLog implements ProTegoDto<ProTegoLog>, Serializable {

    private Type type; // TODO toString
    private double timestamp;
    private ProTegoDto data;

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("type", type.toString());
        jsonObject.put("timestamp", timestamp);
        jsonObject.put("data", data.toJson());

        return jsonObject;
    }

    @Override
    public ProTegoLog fromJson(JSONObject json) throws JSONException, DtoBuilder.DtoBuildException {

        try {
            this.type = Type.valueOf(json.getString("type"));
        } catch (IllegalArgumentException | NullPointerException ex) {
            this.type = Type.RAW;
        }

        this.timestamp = json.getDouble("timestamp");

        JSONObject data = (JSONObject) json.get("data");
        switch (this.type) {
            case KEY_PRESS:
                this.data = new KeyPress();
                break;
            case KEY_RELEASE:
                this.data = new KeyRelease();
                break;
            case GESTURE:
                this.data = new Gesture();
                break;
            case COORDINATES:
                this.data = new Coordinates();
                break;
            case WINDOW_EVENT:
                this.data = new WindowEvent();
                break;
            case RAW:
            default:
                this.data = new Raw();
                data = json; // If its raw, we save the full recevived log
        }

        this.data.fromJson(data);

        return this;
    }

    public enum Type {
        RAW,
        KEY_PRESS,
        KEY_RELEASE,
        GESTURE,
        COORDINATES,
        WINDOW_EVENT
    }

}
