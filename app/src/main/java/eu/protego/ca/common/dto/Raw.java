package eu.protego.ca.common.dto;

import lombok.Data;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

@Data
public class Raw implements ProTegoDto<Raw>, Serializable {

    String data;

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject(this.data);
        return jsonObject;
    }

    @Override
    public Raw fromJson(JSONObject json) throws JSONException {
        this.data = json.toString();
        return this;
    }
}
