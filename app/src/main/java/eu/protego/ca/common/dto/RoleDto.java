package eu.protego.ca.common.dto;

import lombok.Data;
import org.json.JSONException;
import org.json.JSONObject;

@Data
public class RoleDto implements ProTegoDto<RoleDto> {

    private String name;

    public RoleDto() {
    }

    public RoleDto(String name) {
        this.name = name;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", this.name);
        return jsonObject;
    }

    @Override
    public RoleDto fromJson(JSONObject json) throws JSONException {
        this.name = json.getString("name");
        return this;
    }

}
