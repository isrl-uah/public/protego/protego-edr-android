package eu.protego.ca.common.dto;

import org.json.JSONException;
import org.json.JSONObject;

import lombok.Data;

@Data
public class TrustDTO implements ProTegoDto<TrustDTO> {

    double value;
    Precision precision;

    public TrustDTO() {
        // Starts with complete trust, but without any precission
        this.value = 1.0;
        this.precision = Precision.NULL;
    }

    @Override
    public JSONObject toJson() throws JSONException {

        JSONObject res = new JSONObject();
        res.put("value", this.value);

        JSONObject precision = new JSONObject();
        precision.put("name", this.precision.name());
        precision.put("value", this.precision.getValue());
        res.put("precision", precision);

        return res;
    }

    @Override
    public TrustDTO fromJson(JSONObject json) throws JSONException {

        this.value = json.getDouble("value");

        JSONObject precision = json.getJSONObject("precision");
        try {
            this.precision = Precision.valueOf(precision.getString("name"));
        } catch (Exception e) {
            this.precision = Precision.NULL;
        }

        return this;
    }


    // TODO Add names
    /*
    Precission of each model
     */
    public enum Precision {
        NULL(0.0), ABSOLUTE(1.0);

        double value;

        Precision(double value) {
            this.value = value;
        }

        public double getValue() {
            return this.value;
        }

    }
}
