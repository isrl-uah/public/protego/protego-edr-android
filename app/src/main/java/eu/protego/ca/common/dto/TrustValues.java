package eu.protego.ca.common.dto;

import lombok.Data;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@Data
public class TrustValues implements ProTegoDto<TrustValues> {

    List<TrustDTO> values;

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject res = new JSONObject();
        res.put("values", values);
        return res;
    }

    @Override
    public TrustValues fromJson(JSONObject json) throws JSONException, DtoBuilder.DtoBuildException {
        JSONArray values = json.getJSONArray("values");
        this.values = new ArrayList<>();
        for (int i = 0; i < values.length(); i++) {
            JSONObject element = values.getJSONObject(i);
            TrustDTO trust = DtoBuilder.builder(TrustDTO.class).fromJson(element);
            this.values.add(trust);
        }
        return this;
    }
}
