package eu.protego.ca.common.dto;

import lombok.Data;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
public class UserDto implements ProTegoDto<UserDto> {

    private UUID id;
    private String username;
    private String password;
    private boolean enabled;
    private String passwordConfirm;
    private Set<RoleDto> roles;

    public UserDto() {
        this.roles = new HashSet<>();
    }

    public void addRole(RoleDto role) {
        this.roles.add(role);
    }

    @Override
    public JSONObject toJson() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        jsonObject.put("username", username);
        jsonObject.put("enabled", enabled);

        JSONArray roles = new JSONArray();
        for (RoleDto role : this.roles)
            roles.put(role.toJson());

        return jsonObject;
    }

    @Override
    public UserDto fromJson(JSONObject json) throws JSONException, DtoBuilder.DtoBuildException {

        this.id = UUID.fromString(json.getString("id"));
        this.username = json.getString("username");
        this.password = json.getString("password");
        this.enabled = json.getBoolean("enabled");
        this.passwordConfirm = json.getString("passwordConfirm");
        this.roles = new HashSet<>();

        JSONArray roles = json.getJSONArray("roles");

        for (int i = 0; i < roles.length(); i++) {
            RoleDto r = DtoBuilder.builder(RoleDto.class).fromJson(roles.getJSONObject(i));
            this.roles.add(r);
        }

        return this;
    }
}
