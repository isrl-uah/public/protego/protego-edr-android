package eu.protego.ca.common.dto;

import org.json.JSONException;
import org.json.JSONObject;

import lombok.Data;

@Data
public class WindowEvent implements ProTegoDto<WindowEvent> {

    String windowName;
    String elementName;
    double timestamp;
    Type type;

    @Override
    public JSONObject toJson() throws JSONException {

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("windowName", this.windowName);
        jsonObject.put("elementName", this.elementName);
        jsonObject.put("timestamp", this.timestamp);

        JSONObject type = new JSONObject();
        type.put("name", this.type.name());
        type.put("value", this.type.getValue());
        jsonObject.put("type", type);

        return jsonObject;
    }

    @Override
    public WindowEvent fromJson(JSONObject json) throws JSONException, DtoBuilder.DtoBuildException {

        this.windowName = json.getString("windowName");
        this.elementName = json.getString("elementName");
        this.timestamp = json.getDouble("timestamp");

        JSONObject type = json.getJSONObject("type");
        try {
            this.type = Type.valueOf(type.getString("name"));
        } catch (Exception e) {
            this.type = Type.UNKNOWN;
        }

        return this;
    }

    public enum Type {
        OPEN(0), CLOSE(1), UNKNOWN(-1);

        int value;

        Type(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

    }
}
