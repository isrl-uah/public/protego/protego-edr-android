package eu.protego.ca.edr.android.auth;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import javax.annotation.Nullable;


public class AuthContext {

    private static final String PREFS = "prefs";
    private static final String PREF_TOKEN = "jwtToken";
    private static AuthContext sInstance;
    private SharedPreferences mPrefs;

    private AuthContext(@NonNull Context context) {
        mPrefs = context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        sInstance = this;
    }

    public static AuthContext getInstance(@NonNull Context context) {
        if (sInstance == null) {
            sInstance = new AuthContext(context);
        }
        return sInstance;
    }

    @Nullable
    public JWToken getToken() {
        String aux = mPrefs.getString(PREF_TOKEN, null);
        try{
            JWToken token = JWToken.deserialize(aux);

            return aux != null ? token : null;
        } catch (Exception e){
            return null;
        }
    }

    public void saveToken(@NonNull JWToken token) {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(PREF_TOKEN, token.serialize());
        editor.apply();
    }

    public void removeToken() {
        SharedPreferences.Editor editor = mPrefs.edit();
        editor.remove(PREF_TOKEN);
        editor.apply();
    }

    // TODO Validate token (could be caduced)
    public boolean isLoggedIn() {
        try{
            return this.getToken() != null;
        } catch (Exception e){
            return false;
        }
    }

}
