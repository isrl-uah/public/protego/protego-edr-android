package eu.protego.ca.edr.android.auth;

import android.util.Base64;

import lombok.Data;

@Data
public class JWToken {
    private String username;
    private String jwtToken;

    public JWToken() {
    }

    public JWToken(String username, String jwtToken) {
        this.username = username;
        this.jwtToken = jwtToken;
    }

    public static JWToken deserialize(String token) {

        String decoded = new String(Base64.decode(token, Base64.DEFAULT));
        JWToken jwToken = new JWToken();
        jwToken.setUsername(decoded.split(":::")[0]);
        jwToken.setJwtToken(decoded.split(":::")[1]);
        return jwToken;
    }

    public String serialize() {
        return Base64.encodeToString((username + ":::" + jwtToken).getBytes(), Base64.DEFAULT);
    }
}
