package eu.protego.ca.edr.android.com.api;

import rkr.simplekeyboard.inputmethod.BuildConfig;

public class Config {
    /*
        API connection parameters
     */
    public  static final String API = BuildConfig.PROTEGO_EDR_API;

    // Endpoints (EP)
    public static final String TRUST_EP = "/user/trust";
    public static final String USER_EP = "/user";
    public static final String LOGIN_EP = "/login";
    public static final String LOG_EP = "/log";



}
