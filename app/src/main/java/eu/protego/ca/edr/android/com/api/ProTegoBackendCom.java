package eu.protego.ca.edr.android.com.api;

import android.content.Context;

import com.android.volley.Response;

import org.json.JSONObject;

import eu.protego.ca.edr.android.auth.Credentials;
import eu.protego.ca.edr.android.com.api.task.GetTrustTask;
import eu.protego.ca.edr.android.com.api.task.LogTask;
import eu.protego.ca.edr.android.com.api.task.LoginTask;
import eu.protego.ca.common.dto.ProTegoLog;

public class ProTegoBackendCom {

    private Context context;

    public ProTegoBackendCom(Context context) {
        this.context = context;
    }

    // TODO Get user data
    public void getUserData() {
    }

    /**
     * NOPE --> @return Trust    percentaje value [0, 1]
     */
    public <T extends Response.Listener<JSONObject> & Response.ErrorListener> void getTrust(T future) {
        getTrust(future, future);
    }

    public void getTrust(Response.Listener<JSONObject> successCallback, Response.ErrorListener errorCallback) {
        ProTegoBackendComWM queue = ProTegoBackendComWM.getInstance(context);
        GetTrustTask getTrustTask = new GetTrustTask(context, successCallback, errorCallback);
        queue.addTask(getTrustTask);
    }

    public void sendLog(ProTegoLog proTegoLog) {
        // TODO How to get result? https://blogs.oracle.com/corejavatechtips/using-callable-to-return-results-from-runnables
        ProTegoBackendComWM queue = ProTegoBackendComWM.getInstance(context);
        LogTask logTask = new LogTask(context, proTegoLog, null, null);
        queue.addTask(logTask);
    }

    /*
    With <T extends Response.Listener<?> & Response.ErrorListener> we require future implements the two interfaces
     */
    public <T extends Response.Listener<JSONObject> & Response.ErrorListener> void login(Credentials credentials, T future) {
        login(credentials, future, future);
    }

    public void login(Credentials credentials, Response.Listener<JSONObject> successCallback, Response.ErrorListener errorCallback) {
        ProTegoBackendComWM queue = ProTegoBackendComWM.getInstance(context);
        LoginTask loginTask = new LoginTask(context, credentials, successCallback, errorCallback);
        queue.addTask(loginTask);
    }



}
