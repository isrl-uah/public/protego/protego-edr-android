package eu.protego.ca.edr.android.com.api;

import android.content.Context;

import androidx.annotation.NonNull;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import eu.protego.ca.edr.android.com.api.task.ApiTask;

public class ProTegoBackendComWM {

    private static final int KEEP_ALIVE_TIME = 1;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
    private static ProTegoBackendComWM instance;
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private final BlockingQueue<Runnable> queue;
    private final ThreadPoolExecutor pool;

    private ProTegoBackendComWM() {
        this.queue = new LinkedBlockingDeque<>(4);
        this.pool = new ThreadPoolExecutor(NUMBER_OF_CORES, NUMBER_OF_CORES,
                KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, queue);
    }

    protected static ProTegoBackendComWM getInstance(@NonNull Context context) {
        if (instance == null) {
            instance = new ProTegoBackendComWM();
        }
        return instance;
    }

    public void addTask(ApiTask task) {
        this.pool.execute(task);
    }

    // TODO Get user data
    public void getUserData() {
    }

}
