package eu.protego.ca.edr.android.com.api.task;

import com.android.volley.Response;

import org.json.JSONObject;

public interface ApiTask extends Runnable {
    Response.Listener<JSONObject> getSuccessCallback();

    Response.ErrorListener getErrorCallback();
}
