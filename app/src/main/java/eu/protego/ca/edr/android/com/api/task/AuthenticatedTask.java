package eu.protego.ca.edr.android.com.api.task;

import com.android.volley.Response;

import org.json.JSONObject;

import eu.protego.ca.edr.android.com.internal.ProTegoInternalBus;
import eu.protego.ca.edr.android.com.api.ProTegoBackendComExceptions;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessage;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessageType;

public class AuthenticatedTask extends BaseTask implements ApiTask {

    public AuthenticatedTask(Response.Listener<JSONObject> successCallback, Response.ErrorListener errorCallback) {
        super(successCallback, errorCallback);
    }

    @Override
    public Response.ErrorListener getErrorCallback() {
        return error -> {

            AuthenticatedTask.super.getErrorCallback().onErrorResponse(error);

            if (error.getCause() instanceof ProTegoBackendComExceptions.NotLoggedInException) {
                ProTegoInternalMessage proTegoInternalEvent = new ProTegoInternalMessage(ProTegoInternalMessageType.USER_NOT_LOGGED_IN);
                ProTegoInternalBus.log(proTegoInternalEvent);
            }

        };
    }

}
