package eu.protego.ca.edr.android.com.api.task;

import com.android.volley.Response;

import org.json.JSONObject;

public class BaseTask implements ApiTask {

    private Response.Listener<JSONObject> successCallback;
    private Response.ErrorListener errorCallback;

    // TODO Create a chain of callbacks, instead of replacing
    public BaseTask(Response.Listener<JSONObject> successCallback, Response.ErrorListener errorCallback) {
        this.successCallback = successCallback;
        if (successCallback == null)
            this.successCallback = response -> {
            };

        this.errorCallback = errorCallback;
        if (errorCallback == null)
            this.errorCallback = error -> {
            };
    }

    @Override
    public Response.Listener<JSONObject> getSuccessCallback() {
        return this.successCallback;
    }

    @Override
    public Response.ErrorListener getErrorCallback() {
        return this.errorCallback;
    }

    @Override
    public void run() {
    }
}
