package eu.protego.ca.edr.android.com.api.task;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import eu.protego.ca.edr.android.auth.AuthContext;
import eu.protego.ca.edr.android.auth.JWToken;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalBus;
import eu.protego.ca.edr.android.com.api.Config;
import eu.protego.ca.edr.android.com.http.HttpRequest;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessageType;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessage;

public class GetTrustTask extends AuthenticatedTask implements ApiTask {

    private Context context;


    public GetTrustTask(Context context, Response.Listener<JSONObject> successCallback, Response.ErrorListener errorCallback) {
        super(successCallback, errorCallback);
        this.context = context;
    }


    @Override
    public void run() {
        // TODO Clean boilerplates
        HttpRequest httpRequest = null;
        try {
            httpRequest = new HttpRequest(context);
        } catch (HttpRequest.NetworkNotWorking networkNotWorking) {
            getErrorCallback().onErrorResponse(new VolleyError(networkNotWorking));
            return;
        }
        // 1. Authentication
        AuthContext authContext = AuthContext.getInstance(context);
        JWToken token = authContext.getToken();
        if (token == null) {
            ProTegoInternalMessage proTegoInternalEvent = new ProTegoInternalMessage(ProTegoInternalMessageType.USER_NOT_LOGGED_IN);
            ProTegoInternalBus.log(proTegoInternalEvent);
            return;
        }
        httpRequest.setBearerAuth(token.getJwtToken());

        // 2. Get user trust
        httpRequest.get(Config.API + Config.TRUST_EP, null, getSuccessCallback(), getErrorCallback());

    }
}