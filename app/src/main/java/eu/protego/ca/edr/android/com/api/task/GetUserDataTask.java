package eu.protego.ca.edr.android.com.api.task;

import android.content.Context;

import com.android.volley.Response;

import org.json.JSONObject;

public class GetUserDataTask extends AuthenticatedTask implements ApiTask {

    private Context context;

    public GetUserDataTask(Context context, Response.Listener<JSONObject> successCallback, Response.ErrorListener errorCallback) {
        super(successCallback, errorCallback);
        this.context = context;
    }


    @Override
    public void run() {
        // TODO Get data, and create DTO for processing it
    }
}