package eu.protego.ca.edr.android.com.api.task;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import eu.protego.ca.edr.android.auth.AuthContext;
import eu.protego.ca.edr.android.auth.JWToken;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalBus;
import eu.protego.ca.edr.android.com.api.Config;
import eu.protego.ca.edr.android.com.http.HttpRequest;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessageType;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessage;
import eu.protego.ca.common.dto.ProTegoLog;

public class LogTask extends BaseTask implements ApiTask {

    private Context context;
    private ProTegoLog log;

    public LogTask(Context context, ProTegoLog log, Response.Listener<JSONObject> successCallback, Response.ErrorListener errorCallback) {
        super(successCallback, errorCallback);
        this.context = context;
        this.log = log;
    }

    @Override
    public void run() {

        HttpRequest httpRequest = null;
        try {
            httpRequest = new HttpRequest(context);
        } catch (HttpRequest.NetworkNotWorking networkNotWorking) {
            getErrorCallback().onErrorResponse(new VolleyError(networkNotWorking));
            return;
        }

        // 1. Authentication
        AuthContext authContext = AuthContext.getInstance(context);
        JWToken token = authContext.getToken();
        if (token == null) {
            ProTegoInternalMessage proTegoInternalEvent = new ProTegoInternalMessage(ProTegoInternalMessageType.USER_NOT_LOGGED_IN);
            ProTegoInternalBus.log(proTegoInternalEvent);
            return;
        }
        httpRequest.setBearerAuth(token.getJwtToken());

        // TODO 2. Determine type?


        // 3. Send
        try {
            httpRequest.put(Config.API + Config.LOG_EP, log.toJson(), null, getSuccessCallback(), getErrorCallback());
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}
