package eu.protego.ca.edr.android.com.api.task;

import android.content.Context;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import eu.protego.ca.edr.android.auth.Credentials;
import eu.protego.ca.edr.android.com.api.Config;
import eu.protego.ca.edr.android.com.http.HttpRequest;

public class LoginTask extends BaseTask implements ApiTask {

    private Context context;
    private Credentials credentials;

    public LoginTask(Context context, Credentials credentials, Response.Listener<JSONObject> successCallback, Response.ErrorListener errorCallback) {
        super(successCallback, errorCallback);
        this.context = context;
        this.credentials = credentials;
    }

    @Override
    public void run() {

        HttpRequest request = null;
        try {
            request = new HttpRequest(context);
        } catch (HttpRequest.NetworkNotWorking networkNotWorking) {
            getErrorCallback().onErrorResponse(new VolleyError(networkNotWorking));
            return;
        }

        // TODO Create DTO
        JSONObject jsonCredencials = new JSONObject();
        try {
            jsonCredencials.put("username", credentials.getUsername());
            jsonCredencials.put("password", credentials.getPassword());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        request.post(Config.API + Config.LOGIN_EP, jsonCredencials, null, getSuccessCallback(), getErrorCallback());

    }
}
