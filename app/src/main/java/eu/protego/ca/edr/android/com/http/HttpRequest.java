package eu.protego.ca.edr.android.com.http;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import eu.protego.ca.edr.android.auth.Credentials;
import eu.protego.ca.edr.android.com.api.ProTegoBackendComExceptions;


public class HttpRequest {

    private Context context;

    private Map<String, String> headers;
    private boolean authenticated;

    public HttpRequest(Context context) throws NetworkNotWorking {
        this.context = context;
        this.headers = new HashMap<>();
        this.authenticated = false;
    }

    public void setBasicAuth(Credentials credentials) {

    }

    public void setBearerAuth(String token) {
        this.authenticated = true;
        this.headers.put("Authorization", "Bearer " + token);
    }

    // TODO Response.Listener<?> successCallback
    private void sendRequest(int method, String url, @Nullable JSONObject data, @Nullable final Map<String, String> headers, Response.Listener<JSONObject> successCallback,
                             @Nullable Response.ErrorListener errorCallback) {
        Log.d(this.getClass().toString(), "Sending requests");

        final Map<String, String> customHeaders = this.headers;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, url, data, successCallback, error -> {
            if (authenticated)
                if(error.networkResponse != null)
                    if (error.networkResponse.statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        errorCallback.onErrorResponse(new VolleyError(new ProTegoBackendComExceptions.NotLoggedInException()));
                    } else {
                        errorCallback.onErrorResponse(error);
                    }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                // https://www.programming-books.io/essential/android/adding-custom-headers-to-your-requests-e-g-for-basic-auth-c5ce43786eca473c8ad9b1b614eee48f

                // TODO Also call "super.getHeaders()"?
                Map<String, String> requestHeaders = new HashMap<>(); // TODO Create default headers
                if (headers != null)
                    requestHeaders.putAll(headers);

                requestHeaders.putAll(customHeaders);

                return requestHeaders;
            }
        };
        RequestQueueSingleton.getInstance(this.context).addToRequestQueue(jsonObjectRequest);
    }

    public void get(String url, @Nullable final Map<String, String> headers, Response.Listener<JSONObject> successCallback,
                    @Nullable Response.ErrorListener errorCallback) {
        this.sendRequest(Request.Method.GET, url, null, headers, successCallback, errorCallback);
    }

    public void post(String url, @Nullable JSONObject data, @Nullable final Map<String, String> headers, Response.Listener<JSONObject> successCallback,
                     @Nullable Response.ErrorListener errorCallback) {
        this.sendRequest(Request.Method.POST, url, data, headers, successCallback, errorCallback);
    }

    public void put(String url, @Nullable JSONObject data, @Nullable final Map<String, String> headers, Response.Listener<JSONObject> successCallback,
                    @Nullable Response.ErrorListener errorCallback) {
        this.sendRequest(Request.Method.PUT, url, data, headers, successCallback, errorCallback);
    }

    public class NetworkNotWorking extends Exception {
    }
}
