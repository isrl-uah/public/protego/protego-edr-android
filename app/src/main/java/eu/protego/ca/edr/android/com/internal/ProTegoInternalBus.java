package eu.protego.ca.edr.android.com.internal;

import org.greenrobot.eventbus.EventBus;

public class ProTegoInternalBus {

    public static void log(ProTegoInternalMessage event) {
        EventBus.getDefault().post(event);
    }

}
