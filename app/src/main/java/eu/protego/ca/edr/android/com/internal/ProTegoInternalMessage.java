package eu.protego.ca.edr.android.com.internal;

import java.io.Serializable;

/**
 * Internal application's events
 */
public class ProTegoInternalMessage implements Serializable {

    private ProTegoInternalMessageType type;
    private Object data;

    public ProTegoInternalMessage(ProTegoInternalMessageType type) {
        this.type = type;
        this.data = null;
    }

    public ProTegoInternalMessage(ProTegoInternalMessageType type, Object data) {
        this.type = type;
        this.data = data;
    }

    public ProTegoInternalMessageType getType() {
        return type;
    }

    public void setType(ProTegoInternalMessageType type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
