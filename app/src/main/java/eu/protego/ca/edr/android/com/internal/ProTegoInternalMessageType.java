package eu.protego.ca.edr.android.com.internal;

// TODO
// - The set of events we manage is essential
// - Difference Internal / External events
// Internal events
public enum ProTegoInternalMessageType {
    LOG,
    USER_NOT_LOGGED_IN,
    TRUST_DATA, // Update application's trust,
    EVAL_TRUST, // Get trust from the server
    RESET_CA_DATA,
    AUTHENTICATION_IMPERSONATED, // No trust in user's identity
    CHECK_STATUS, // Ask services for their status
    SHOW_STATUS; // Answer the status of a service
}
