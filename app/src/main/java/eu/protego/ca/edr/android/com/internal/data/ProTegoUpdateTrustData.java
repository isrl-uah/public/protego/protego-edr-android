package eu.protego.ca.edr.android.com.internal.data;

import eu.protego.ca.common.dto.TrustDTO;
import lombok.Data;

@Data
public class ProTegoUpdateTrustData {

    private TrustDTO trust;

    public ProTegoUpdateTrustData(TrustDTO trust) {
        this.trust = trust;
    }

    public String toString() {
        return String.valueOf(this.trust.getValue());
    }
    
}
