package eu.protego.ca.edr.android.connectors;

import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import eu.protego.ca.edr.android.com.internal.ProTegoInternalBus;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessage;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessageType;
import eu.protego.ca.common.dto.Gesture;
import eu.protego.ca.common.dto.ProTegoLog;
import eu.protego.ca.common.dto.WindowEvent;

public class ProTegoAccessibilityConnector implements ProTegoConnector {

    public static void newGestureEvent(int gestureId) {

        Gesture gesture = new Gesture();
        gesture.setGestureCode(gestureId);
        gesture.setTimestamp(System.currentTimeMillis());

        ProTegoLog proTegoLog = new ProTegoLog();
        proTegoLog.setData(gesture);
        proTegoLog.setType(ProTegoLog.Type.GESTURE);

        ProTegoInternalMessage event = new ProTegoInternalMessage(ProTegoInternalMessageType.LOG, proTegoLog);
        ProTegoInternalBus.log(event);

    }

    public static void newKeyEvent(KeyEvent kevt) {
    }

    public static void newAccessibilityEvent(AccessibilityEvent aevt) {
        switch (aevt.getEventType()) {
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                windowStateChange(aevt);
                break;
        }
    }

    private static void windowStateChange(AccessibilityEvent aevt) {

        AccessibilityNodeInfo nodeInfo = aevt.getSource();

        if (nodeInfo == null) {
            return;
        }

        String packageName = nodeInfo.getPackageName().toString();
        String elementName = aevt.getClassName().toString();

        WindowEvent windowEvent = new WindowEvent();
        windowEvent.setWindowName(packageName);
        windowEvent.setElementName(elementName);
        windowEvent.setTimestamp(System.currentTimeMillis());
        windowEvent.setType(WindowEvent.Type.UNKNOWN);

        ProTegoLog proTegoLog = new ProTegoLog();
        proTegoLog.setType(ProTegoLog.Type.WINDOW_EVENT);
        proTegoLog.setData(windowEvent);

        ProTegoInternalMessage event = new ProTegoInternalMessage(ProTegoInternalMessageType.LOG, proTegoLog);
        ProTegoInternalBus.log(event);


    }
}
