package eu.protego.ca.edr.android.connectors;

import eu.protego.ca.edr.android.com.internal.ProTegoInternalBus;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessageType;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessage;
import eu.protego.ca.common.dto.KeyPress;
import eu.protego.ca.common.dto.KeyRelease;
import eu.protego.ca.common.dto.ProTegoLog;
import rkr.simplekeyboard.inputmethod.keyboard.Key;

/**
 * Generate keyboard logs from keyboard events
 */
public class ProTegoKeyboardConnector implements ProTegoConnector {

    public static void keyPressed(Key k) {

        KeyPress kr = new KeyPress();
        kr.setKeyCode(k.getCode());
        kr.setTimestamp(System.currentTimeMillis());

        ProTegoLog proTegoLog = new ProTegoLog();
        proTegoLog.setData(kr);
        proTegoLog.setType(ProTegoLog.Type.KEY_PRESS);

        ProTegoInternalMessage event = new ProTegoInternalMessage(ProTegoInternalMessageType.LOG, proTegoLog);
        ProTegoInternalBus.log(event);

    }

    public static void keyReleased(Key k) {

        KeyRelease kr = new KeyRelease();
        kr.setKeyCode(k.getCode());
        kr.setTimestamp(System.currentTimeMillis());

        ProTegoLog proTegoLog = new ProTegoLog();
        proTegoLog.setData(kr);
        proTegoLog.setType(ProTegoLog.Type.KEY_RELEASE);

        ProTegoInternalMessage event = new ProTegoInternalMessage(ProTegoInternalMessageType.LOG, proTegoLog);
        ProTegoInternalBus.log(event);

    }

}
