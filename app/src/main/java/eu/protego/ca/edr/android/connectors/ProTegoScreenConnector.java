package eu.protego.ca.edr.android.connectors;

import android.view.MotionEvent;

import eu.protego.ca.edr.android.com.internal.ProTegoInternalBus;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessageType;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessage;
import eu.protego.ca.common.dto.Coordinates;
import eu.protego.ca.common.dto.ProTegoLog;

public class ProTegoScreenConnector implements ProTegoConnector {

    public static void touch(MotionEvent motionEvent) {

        // https://developer.android.com/reference/android/view/MotionEvent
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_UP:
                pressureEvent(motionEvent);
                break;
        }

    }

    static void pressureEvent(MotionEvent motionEvent) {

        float x = motionEvent.getRawX();
        float y = motionEvent.getRawY();

        Coordinates coordinates = new Coordinates();
        coordinates.setTimestamp(System.currentTimeMillis());
        coordinates.setX(x);
        coordinates.setY(y);
        // TODO Add type (UP/DOWN) information?

        ProTegoLog proTegoLog = new ProTegoLog();
        proTegoLog.setData(coordinates);
        proTegoLog.setType(ProTegoLog.Type.COORDINATES);

        ProTegoInternalMessage event = new ProTegoInternalMessage(ProTegoInternalMessageType.LOG, proTegoLog);
        ProTegoInternalBus.log(event);

    }

}
