package eu.protego.ca.edr.android.core;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;

import eu.protego.ca.common.dto.DtoBuilder;
import eu.protego.ca.common.dto.ProTegoLog;
import eu.protego.ca.common.dto.TrustDTO;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalBus;
import eu.protego.ca.edr.android.com.api.ProTegoBackendCom;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessage;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessageType;
import eu.protego.ca.edr.android.com.internal.data.ProTegoUpdateTrustData;

public class ProTegoCore {

    private Context context;

    private ProTegoBackendCom proTegoBackendCom;

    public ProTegoCore(Context context) {
        this.context = context;
        this.proTegoBackendCom = new ProTegoBackendCom(context);
    }

    public void evalTrust() {

        // TODO Refactor
        proTegoBackendCom.getTrust(response -> {

            TrustDTO trust = new TrustDTO();
            trust.setPrecision(TrustDTO.Precision.ABSOLUTE);
            trust.setValue(-1.0); // Undefined

            try {
                trust = DtoBuilder.builder(TrustDTO.class).fromJson(response);
            } catch (DtoBuilder.DtoBuildException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // Send back to the system
            ProTegoUpdateTrustData proTegoUpdateTrustData = new ProTegoUpdateTrustData(trust);
            ProTegoInternalMessage evt = new ProTegoInternalMessage(ProTegoInternalMessageType.TRUST_DATA, proTegoUpdateTrustData);
            ProTegoInternalBus.log(evt);

        }, error -> {
            TrustDTO trust = new TrustDTO();
            trust.setPrecision(TrustDTO.Precision.ABSOLUTE);
            trust.setValue(-1.0); // Undefined

            // Send back to the system
            ProTegoUpdateTrustData proTegoUpdateTrustData = new ProTegoUpdateTrustData(trust);
            ProTegoInternalMessage evt = new ProTegoInternalMessage(ProTegoInternalMessageType.TRUST_DATA, proTegoUpdateTrustData);
            ProTegoInternalBus.log(evt);

        });
    }

    public void processEvent(ProTegoInternalMessage event) {

        Log.e("EVENT", event.toString());

        switch (event.getType()) {
            case EVAL_TRUST:
                evalTrust();
                break;
            case LOG:
                processLog((ProTegoLog) event.getData());
                break;
            default:
                break;
        }
    }


    private void processLog(ProTegoLog proTegoLog) {
        proTegoBackendCom.sendLog(proTegoLog);
    }
}