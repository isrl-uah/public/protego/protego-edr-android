package eu.protego.ca.edr.android.core;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import eu.protego.ca.edr.android.auth.AuthContext;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalBus;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessage;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessageType;
import eu.protego.ca.edr.android.com.internal.data.ProTegoUpdateTrustData;
import eu.protego.ca.edr.android.core.receiver.ProTegoCoreServiceReceiver;
import eu.protego.ca.edr.android.services.UpdateTrustService;
import rkr.simplekeyboard.inputmethod.R;

// TODO Review https://developer.android.com/guide/components/services#ExtendingService
public class ProTegoCoreService extends Service {

    public static final String CHANNEL_ID = "ProTegoCoreService";
    public static final int NOTIFICATION_ID = 1;
    private ProTegoCore core;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void processEvent(ProTegoInternalMessage event) {

        switch (event.getType()) {
            // CoreService own events
            case TRUST_DATA:
                ProTegoUpdateTrustData trustData = (ProTegoUpdateTrustData) event.getData();
                this.setTrust(trustData.getTrust().getValue());
                break;
            case USER_NOT_LOGGED_IN:
                AuthContext.getInstance(getApplicationContext()).removeToken();
                handleNotLoggedIn();
                break;
            case AUTHENTICATION_IMPERSONATED:
                authenticationImpersonatedHandler();
                break;
        }

    }


    /**
     * Handle authentication impersonation
     */
    private void authenticationImpersonatedHandler() {
        ScreenLocker.getScreenLockerSingleton(getApplicationContext()).lockScreen();
    }

    /**
     * TODO Write not loggedIn logic
     */
    public void handleNotLoggedIn() {

    }

    /*
     Event Bus connection
     */
    @Subscribe(threadMode = ThreadMode.BACKGROUND)
    public void onMessage(ProTegoInternalMessage event) {
        // Every event passes through the core
        core.processEvent(event);
        this.processEvent(event);
        Log.e("PMS", event.getType() + " : " + event.getData());
    }

    /**
     * Method for ensuring the service survivance. It creates an alarm that wakes up the
     * service, for just in case it gets closed by the system
     */
    private void serviceChecker() {

        final int HALF_MINUTE = 30 * 1000;

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent nextIntent = new Intent(this, ProTegoCoreService.class);

        PendingIntent pendingIntent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            pendingIntent = PendingIntent.getForegroundService(this, 0, nextIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        } else {
            pendingIntent = PendingIntent.getService(this, 0, nextIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        }

        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, HALF_MINUTE, pendingIntent);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // Launch the serviceChecker alarm
        serviceChecker();

        // If core != null, the service is already running
        if (this.core != null)
            return START_STICKY; // What to return here?

        Log.d(this.getClass().toString(), "Hello!");


        // Starting core
        this.core = new ProTegoCore(this);


        /**
         * Try to move all the logic to ProTegoCore
         */
        Intent notificationIntent = new Intent(this, ProTegoCoreService.class);
        createNotificationChannel();


        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("ProTego")
                .setContentText("Continuous Authentication Service")
                .setSmallIcon(R.drawable.protego_logo_color)
                .setContentIntent(pendingIntent)
                .build();

        // Connection with EventBus
        try {
            EventBus.getDefault().register(this);
        } catch (Exception e) {
            Log.d(this.getClass().toString(), e.toString());
        }

        startForeground(NOTIFICATION_ID, notification);

        // Trust evaluation logic
        setTrust(-1.0);

        // Start trust updater
        Intent loginIntent = new Intent(this, UpdateTrustService.class);
        startService(loginIntent);

        return START_STICKY;
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "ProTego Service Channel", NotificationManager.IMPORTANCE_NONE);
            // channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void setTrust(double trust) {
        /**
         * If the trust data is less than 0, it is indeterminate
         */
        setTrust(trust, trust < 0);
        if (trust < 0.5 && trust >= 0)
            ProTegoInternalBus.log(new ProTegoInternalMessage(ProTegoInternalMessageType.AUTHENTICATION_IMPERSONATED));
    }

    private void setTrust(double trust, boolean indeterminate) {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("ProTego")
                .setContentText("Continuous Authentication Service")
                .setSmallIcon(R.drawable.protego_logo_color);

        // Issue the initial notification with zero progress
        int PROGRESS_MAX = 100;

        builder.setProgress(PROGRESS_MAX, (int) (PROGRESS_MAX * trust), indeterminate);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    @Override
    public void onDestroy() {

        Log.d(this.getClass().toString(), "Bye!");

        ProTegoCoreServiceReceiver.call(this);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
        stopSelf();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
