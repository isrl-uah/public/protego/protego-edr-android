package eu.protego.ca.edr.android.core;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import eu.protego.ca.edr.android.core.receiver.ProTegoDeviceAdminReceiver;

public class ScreenLocker {

    static ScreenLocker screenLocker = null;

    Context context;
    DevicePolicyManager devicePolicyManager;
    ComponentName compName;
    boolean locked = false;

    public static ScreenLocker getScreenLockerSingleton(Context context) {
        if (screenLocker == null)
            screenLocker = new ScreenLocker(context);

        return screenLocker;
    }

    ScreenLocker(Context context) {
        this.context = context;
        devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        compName = new ComponentName(context, ProTegoDeviceAdminReceiver.class);
    }

    public void lockScreen() {
        if (this.locked)
            return;

        boolean active = devicePolicyManager.isAdminActive(compName);

        if (active) {
            devicePolicyManager.lockNow();
        } else {

            Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, compName);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "Additional text explaining why we need this permission");
            context.startActivity(intent);
            return;
        }

        this.locked = true;
    }
}
