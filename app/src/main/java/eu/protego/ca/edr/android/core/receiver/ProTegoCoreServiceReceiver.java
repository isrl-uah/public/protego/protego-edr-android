package eu.protego.ca.edr.android.core.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.content.ContextCompat;

import eu.protego.ca.edr.android.core.ProTegoCoreService;

/**
 * The receiver is necessary for launching the background service
 */
public class ProTegoCoreServiceReceiver extends BroadcastReceiver {

    public static void call(Context context){
        Intent receiverIntent = new Intent(context, ProTegoCoreServiceReceiver.class);
        context.sendBroadcast(receiverIntent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // Start notification service
        Intent msgService = new Intent(context, ProTegoCoreService.class);
        ContextCompat.startForegroundService(context, msgService);
    }
}
