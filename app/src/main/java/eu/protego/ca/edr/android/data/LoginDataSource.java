package eu.protego.ca.edr.android.data;

import android.content.Context;

import com.android.volley.toolbox.RequestFuture;

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import eu.protego.ca.edr.android.auth.AuthContext;
import eu.protego.ca.edr.android.auth.Credentials;
import eu.protego.ca.edr.android.auth.JWToken;
import eu.protego.ca.edr.android.com.api.ProTegoBackendCom;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    private Context context;
    private AuthContext authContext;

    public LoginDataSource(Context context) {
        this.context = context;
        authContext = AuthContext.getInstance(context);
    }

    public Result<JWToken> login(String username, String password) {

        if (authContext.isLoggedIn()) {
            authContext.removeToken();
        }

        ProTegoBackendCom proTegoBackendCom = new ProTegoBackendCom(context);

        Credentials credentials = new Credentials();
        credentials.setUsername(username);
        credentials.setPassword(password);
        final RequestFuture<JSONObject> future = RequestFuture.newFuture();

        proTegoBackendCom.login(credentials, future);
        try {

            JSONObject response = future.get(3, TimeUnit.SECONDS);

            JWToken token = new JWToken();
            token.setUsername(credentials.getUsername());
            token.setJwtToken(response.getString("jwtToken"));

            return new Result.Success<>(token);
        } catch (Exception e) {
            return new Result.Error(e);
        }
    }

    public void logout() {
        authContext.removeToken();
    }

    public boolean isLoggedIn() {
        return authContext.isLoggedIn();
    }
}
