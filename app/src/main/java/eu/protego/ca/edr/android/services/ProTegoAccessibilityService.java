package eu.protego.ca.edr.android.services;

import android.accessibilityservice.AccessibilityService;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;

import eu.protego.ca.edr.android.connectors.ProTegoAccessibilityConnector;

public class ProTegoAccessibilityService extends AccessibilityService {

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        ProTegoAccessibilityConnector.newAccessibilityEvent(event);
    }

    @Override
    protected boolean onGesture(int gestureId) {
        ProTegoAccessibilityConnector.newGestureEvent(gestureId);
        return false;
    }

    @Override
    protected boolean onKeyEvent(KeyEvent event) {
        return false;
    }

    @Override
    public void onInterrupt() {
    }

}
