package eu.protego.ca.edr.android.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;

import eu.protego.ca.edr.android.com.internal.ProTegoInternalBus;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessage;
import eu.protego.ca.edr.android.com.internal.ProTegoInternalMessageType;

public class UpdateTrustService extends IntentService {
    private static final int MINUTE = 60 * 1000; //60 * 1000;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public UpdateTrustService(String name) {
        super(name);
    }

    public UpdateTrustService() {
        super(null);
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        // TODO Check CoreService is running?
        ProTegoInternalMessage event = new ProTegoInternalMessage(ProTegoInternalMessageType.EVAL_TRUST);
        ProTegoInternalBus.log(event);

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        Intent nextIntent = new Intent(this, UpdateTrustService.class);
        PendingIntent pendingIntent = PendingIntent.getService(this, 0, nextIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, MINUTE, pendingIntent);
    }
}
