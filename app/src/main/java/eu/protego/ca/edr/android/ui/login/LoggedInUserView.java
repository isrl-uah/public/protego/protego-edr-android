package eu.protego.ca.edr.android.ui.login;

import eu.protego.ca.edr.android.auth.JWToken;

/**
 * Class exposing authenticated user details to the UI.
 */
class LoggedInUserView {
    private String username;
    private JWToken jwtToken;
    //... other data fields that may be accessible to the UI

    LoggedInUserView(String username, JWToken jwtToken) {
        this.username = username;
        this.jwtToken = jwtToken;
    }

    JWToken getJwtToken() {
        return jwtToken;
    }

    String getUsername() {
        return username;
    }

    String getDisplayName() {
        return username;
    }
}
