package eu.protego.ca.edr.android.ui.login;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import eu.protego.ca.edr.android.auth.AuthContext;
import eu.protego.ca.edr.android.auth.Credentials;
import eu.protego.ca.edr.android.core.receiver.ProTegoCoreServiceReceiver;
import rkr.simplekeyboard.inputmethod.R;

public class ProTegoLogonActivity extends AppCompatActivity {

    private LoginViewModel loginViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.getBoolean(Extras.BAD_STORED_CREDENTIALS.val()))
                Toast.makeText(this, R.string.BAD_STORED_CREDENTIALS, Toast.LENGTH_LONG).show();
        }

        setContentView(R.layout.activity_pro_tego_login);

        loginViewModel = ViewModelProviders.of(this, new LoginViewModelFactory(getApplicationContext()))
                .get(LoginViewModel.class);

        final EditText usernameEditText = findViewById(R.id.username);
        final EditText passwordEditText = findViewById(R.id.password);
        final Button loginButton = findViewById(R.id.login);
        final ProgressBar loadingProgressBar = findViewById(R.id.loading);

        loginViewModel.getLoginFormState().observe(this, new Observer<LoginFormState>() {
            @Override
            public void onChanged(@Nullable LoginFormState loginFormState) {
                if (loginFormState == null) {
                    return;
                }
                loginButton.setEnabled(loginFormState.isDataValid());
                if (loginFormState.getUsernameError() != null) {
                    usernameEditText.setError(getString(loginFormState.getUsernameError()));
                }
                if (loginFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(loginFormState.getPasswordError()));
                }
            }
        });

        loginViewModel.getLoginResult().observe(this, new Observer<LoginResult>() {
            @Override
            public void onChanged(@Nullable LoginResult loginResult) {
                if (loginResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (loginResult.getError() != null) {
                    showLoginFailed(loginResult.getError());
                }
                if (loginResult.getSuccess() != null) {
                    updateUiWithUser(loginResult.getSuccess());
                    setResult(Activity.RESULT_OK);

                    //Complete and destroy login activity once successful
                    finish();
                }
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // ignore
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // ignore
            }

            @Override
            public void afterTextChanged(Editable s) {
                loginViewModel.loginDataChanged(usernameEditText.getText().toString(),
                        passwordEditText.getText().toString());
            }
        };
        usernameEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loadingProgressBar.setVisibility(View.VISIBLE);
                    Credentials credentials = new Credentials();
                    credentials.setUsername(usernameEditText.getText().toString());
                    credentials.setPassword(passwordEditText.getText().toString());
                    new LoginOperation().execute(credentials);
                }
                return false;
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                Credentials credentials = new Credentials();
                credentials.setUsername(usernameEditText.getText().toString());
                credentials.setPassword(passwordEditText.getText().toString());
                new LoginOperation().execute(credentials);
            }
        });

        // Notify ProTegoCoreServiceReceiver
        ProTegoCoreServiceReceiver.call(getApplicationContext());
    }

    private void updateUiWithUser(LoggedInUserView model) {
        String welcome = getString(R.string.welcome) + " " + model.getDisplayName() + "!";
        // TODO Where should it be saved?
        AuthContext.getInstance(getApplicationContext()).saveToken(model.getJwtToken());
        // TODO : initiate successful logged in experience
        Toast.makeText(getApplicationContext(), welcome, Toast.LENGTH_LONG).show();
    }

    private void showLoginFailed(@StringRes Integer errorString) {
        Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_LONG).show();
    }

    public enum Extras {
        BAD_STORED_CREDENTIALS;

        public String val() {
            return this.name();
        }
    }

    private class LoginOperation extends AsyncTask<Credentials, Integer, Void> {
        protected Void doInBackground(Credentials... credentialsArray) {
            Credentials credentials = credentialsArray[0];
            loginViewModel.login(credentials.getUsername(), credentials.getPassword());
            return null;
        }
    }
}