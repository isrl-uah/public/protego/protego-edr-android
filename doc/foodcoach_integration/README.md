# Integration FoodCoach-UAH

## IAM

The JWT format should be the next:

```js
{
  "aud": "https://edr.protego.junquera.xyz",
  "exp": "xxxx", // Epoch (s)
  "jti": "ad2febe8-a2b6-4ffe-8e10-af387b7440a7",
  "iat": "xxxx", // Epoch (s)
  "sub": "username",
  "iss": "https://iam.foodcoach.com",
  "nbf": "xxxx" // Epoch (s)
}
```

Even though the ideal would be to fill all the claims, `jti` and `nbf` are not mandatory. The fields that must be filled are:

- `aud`: Indicating we (the EDR's backed) are the audience of the token

- `exp`, `iat`: You have them yet

- `sub`: You have it also yet, but you put the identity of the user in an `id` field.

The last required parameter is `iss`. We should know the issuer's identity (IP, URL, or any other identifier) in order to validate the token. We can follow two stretegies to valiate the token:

- You could provide us the public keys corresponding to those used to sign the token

- The most desirable approach is that the `iss` content was the URL of your IAM, so we could use a JWK to retrieve your keys.

## Flow diagram

Authentication would work like this:

![Authentication flow](auth_flow.png)
